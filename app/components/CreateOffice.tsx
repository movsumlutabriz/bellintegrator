import * as React from 'react';
import {Redirect} from 'react-router-dom';

export default class CreateOffice extends React.Component<any,{}>{
    constructor(props:any){
        super(props)
    }

    name:HTMLInputElement;
    address:HTMLInputElement;
    phone:HTMLInputElement;

    handleFormCreateOffice = (event:any) =>{
        let dataInputs = {
            name:this.name.value,
            address:this.address.value,
            phone:this.phone.value
        }
        event.preventDefault();
        this.props.sendCreatedOffice(dataInputs);
    }

    componentWillUnmount(){
        this.props.createdOff()
    }

    render(){
        if(this.props.createdOffice){
            return(
                <Redirect to="/offices"/>
            )
        }

        return(
            <div className="createOffice-container">
                <div className="createOffice-head">
                    <h1>добавить новый офис</h1>
                </div>

                <form className="createOffice-block">
                <div>
                    <span>Название: </span>
                    <input type="text" ref={(node)=>{this.name = node}}/>
                    <span>Адрес: </span>
                    <input type="text" ref={(node)=>{this.address = node}}/>
                    <span>Телефон: </span>
                    <input type="text" ref={(node)=>{this.phone = node}}/>
                </div>

                <input className="button-create__office" type="submit" value="Создать" onClick={this.handleFormCreateOffice}/>
                </form>
            </div>
        )
    }
}
