import * as React from 'react';
import {Redirect} from 'react-router-dom';

export default class CreateOrg extends React.Component<any,{}>{
    constructor(props:any){
        super(props)
    }

    name:HTMLInputElement;
    inn:HTMLInputElement;
    kpp:HTMLInputElement;
    address:HTMLInputElement;
    phone:HTMLInputElement;

    handleFormCreateOrg = (event:any) =>{
        let dataInputs = {
            name:this.name.value,
            inn:this.inn.value,
            kpp:this.kpp.value,
            address:this.address.value,
            phone:this.phone.value
        }
        event.preventDefault();
        this.props.sendCreatedOrg(dataInputs);
    }

    componentWillUnmount(){
        this.props.createdOrganization()
    }

    render(){
        if(this.props.createdOrg){
            return(
                <Redirect to="/organizations"/>
            )
        }

        return(
            <div className="createOrg-container">
                <div className="createOrg-head">
                    <h1>добавить организацию</h1>
                </div>

                <form className="createOrg-block">
                    <div>
                        <span>Название: </span>
                        <input type="text" ref={(node)=>{this.name = node}}/>
                        <span>ИНН: </span>
                        <input type="text" ref={(node)=>{this.inn = node}}/>
                        <span>КПП: </span>
                        <input type="text" ref={(node)=>{this.kpp = node}}/>
                        <span>Адрес: </span>
                        <input type="text" ref={(node)=>{this.address = node}}/>
                        <span>Телефон: </span>
                        <input type="text" ref={(node)=>{this.phone = node}}/>
                    </div>
                    <input className="button-create__org" type="submit" value="Создать" onClick={this.handleFormCreateOrg}/>
                </form>
            </div>
        )
    }
}
