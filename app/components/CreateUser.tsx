import * as React from 'react';
import {Redirect} from 'react-router-dom';

export default class CreateUser extends React.Component<any,{}>{
    constructor(props:any){
        super(props)
    }

    firstName:HTMLInputElement;
    secondName:HTMLInputElement;
    middleName:HTMLInputElement;
    position:HTMLInputElement;
    phone:HTMLInputElement;
    docName:HTMLInputElement;
    docNumber:HTMLInputElement;
    docDate:HTMLInputElement;
    citizenshipName:HTMLInputElement;
    citizenshipCode:HTMLInputElement;
    isIdentified:HTMLInputElement;

    handleFormCreateUser = (event:any) =>{
        let dataInputs = {
            firstName:this.firstName.value,
            secondName:this.secondName.value,
            middleName:this.middleName.value,
            position:this.position.value,
            phone:this.phone.value,
            docName:this.docName.value,
            docNumber:this.docNumber.value,
            docDate:this.docDate.value,
            citizenshipName:this.citizenshipName.value,
            citizenshipCode:this.citizenshipCode.value,
            isIdentified:this.isIdentified.value
        }
        event.preventDefault();
        this.props.sendCreatedUser(dataInputs);
    }

    componentWillUnmount(){
        this.props.createdUsr()
    }

    render(){
        if(this.props.createdUser){
            return(
                <Redirect to="/users"/>
            )
        }

        return(
            <div className="createUser-container">
                <div className="createUser-head">
                    <h1>добавить нового сотрудника</h1>
                </div>

                <form className="createUser-block">
                    <div>
                        <span>Имя: </span>
                        <input type="text" ref={(node)=>{this.firstName = node}}/>
                        <span>Фамилия: </span>
                        <input type="text" ref={(node)=>{this.secondName = node}}/>
                        <span>Отчество: </span>
                        <input type="text" ref={(node)=>{this.middleName = node}}/>
                        <span>Должность: </span>
                        <input type="text" ref={(node)=>{this.position = node}}/>
                        <span>Телефон: </span>
                        <input type="text" ref={(node)=>{this.phone = node}}/>
                        <span>docName: </span>
                        <input type="text" ref={(node)=>{this.docName = node}}/>
                        <span>docNumber: </span>
                        <input type="text" ref={(node)=>{this.docNumber = node}}/>
                        <span>docDate: </span>
                        <input type="text" ref={(node)=>{this.docDate = node}}/>
                        <span>citizenshipName: </span>
                        <input type="text" ref={(node)=>{this.citizenshipName = node}}/>
                        <span>citizenshipCode: </span>
                        <input type="text" ref={(node)=>{this.citizenshipCode = node}}/>
                        <span>isIdentified: </span>
                        <input type="text" ref={(node)=>{this.isIdentified = node}}/>
                    </div>

                    <input className="button-create__user" type="submit" value="cоздать" onClick={this.handleFormCreateUser}/>
                </form>
            </div>
        )
    }
}
