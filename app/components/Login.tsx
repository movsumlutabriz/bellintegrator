import * as React from 'react';
import {Redirect} from 'react-router-dom';

export default class Login extends React.Component<any, {}>{
    constructor(props:any){
        super(props)

    }
    login:HTMLInputElement;
    password:HTMLInputElement;


    handlerSubmitForm = (event:any)=>{
        event.preventDefault();
        this.props.onSubmitForm(this.login,this.password)
    }

    render() {
        if(this.props.isLogin){
            return <Redirect from="/" to="/organizations"/>
        }
        return (
            <div className="login-container">
                <div className="login-head">
                    <h1>вход в систему</h1>
                </div>

                <form className="login-block">
                    <div>
                        <span>ваш логин: </span>
                        <input ref={node=>{this.login = node}} type="text" id="login"/>
                        <span>ваш пароль: </span>
                        <input ref={node=>{this.password = node}} type="text" id="password"/>
                    </div>

                    <input className="button-login" type="submit" value="вход" onClick={this.handlerSubmitForm}/>
                </form>
            </div>
        )
    }
}
