import * as React from 'react';
import {Link} from 'react-router-dom';

const OfficeBlock = (office:any,{onclick}:any) => {
    return(
    <div className="office_item">
        <div className="office-block__head">
            <h2>{office.name}</h2>
        </div>
        <div className="office-body">
            <p>адрес:{office.address}</p>
            <p>телефон:{office.phone}</p>
        </div>
        <div className="office-buttons">
            <Link to="/users">сотрудники</Link>
            <Link to={`/updateoffice/${office.id}`}>изменить</Link>
            <a href="" onClick={onclick}>Удалить</a>
        </div>

    </div>
    )
};

export default class Offices extends React.Component<any>{
    constructor(props:any){
        super(props)

    }

    onClickDelete = ()=>{
        return this.props.deleteOffice
    }

    componentWillMount(){
        this.props.loadOffices()
    }

    render() {
        return(
            <div className="office-container">
                <div className="office-head">
                    <div>
                        <h1>список оффисов</h1>
                    </div>
                    <div className="office-create">
                        <Link to="/createoffice">добавить офис</Link>
                    </div>
                </div>


                <div className="office_block">
                    {this.props.offices.map((office:any)=>{
                        return <OfficeBlock key={office.id} {...office} onclick={()=>this.onClickDelete()}/>
                    })}
                </div>
            </div>

        )
    }
}
