import * as React from 'react';
import {Link} from 'react-router-dom';

const OrgBlock = (org:any,{onclick}:any) => {
    return(
    <div className="org-item">
        <div className="org-block__head">
                <h2>{org.name}</h2>
        </div>
        <div className="org-body">
            <p>ИНН:{org.inn}</p>
            <p>КПП:{org.kpp}</p>
            <p>Адрес:{org.address}</p>
            <p>Телефон:{org.phone}</p>
        </div>


        <div className="link-to__office">
            <Link to="/offices">офисы</Link>
            <Link to={`/updateorg/${org.id}`}>изменить</Link>
            <a href="" onClick={onclick}>удалить</a>
        </div>

    </div>
    )
};

export default class Organizations extends React.Component<any>{
    constructor(props:any){
        super(props)

    }

    onClickDelete = ()=>{
        return this.props.deleteOrganization
    }

    componentWillMount(){
        this.props.loadOrganizations()
    }

    render() {
        return(
            <div className="org-container">
                <div className="org-head">
                    <div>
                        <h1>список организаций</h1>
                    </div>

                    <div className="org-create">
                        <Link to="/createorg">добавить организацию</Link>
                    </div>
                </div>

                <div className="org_block">
                    {this.props.orgs.map((org:any)=>{
                        return <OrgBlock key={org.id} {...org} onclick={()=>this.onClickDelete()}/>
                    })}
                </div>
            </div>

        )
    }
}
