import * as React from 'react';
import {Redirect} from 'react-router-dom';

export default class UpdateOffice extends React.Component<any,{}>{
    constructor(props:any){
        super(props)
    }

    name:HTMLInputElement;
    address:HTMLInputElement;
    phone:HTMLInputElement;

    handleFormUpdateOffice = (event:any) =>{
        let dataInputs = {
            name:this.name.value,
            address:this.address.value,
            phone:this.phone.value
        }
        event.preventDefault();
        this.props.sendChanges(dataInputs);
    }

    componentDidMount(){
        this.props.loadOffice()
    }

    componentWillUnmount(){
        this.props.changeIsUpdated()
    }

    render(){
        const defaultVal = this.props.officeForUpdate;

        if(this.props.officeIsUpdated){
            return(
                <Redirect to="/offices"/>
            )
        }

        return(
            <div className="updateOffice-container">
                <div className="updateOffice-head">
                    <h1>изменение данных оффиса</h1>
                </div>

                <form className="updateOffice-block">
                    <div>
                        <span>Название: </span>
                        <input type="text" defaultValue={defaultVal.name}
                        placeholder={defaultVal.name} ref={(node)=>{this.name = node}}/>
                        <span>Адрес: </span>
                        <input type="text" defaultValue={defaultVal.address}
                        placeholder={defaultVal.address} ref={(node)=>{this.address = node}}/>
                        <span>Телефон: </span>
                        <input type="text" defaultValue={defaultVal.phone}
                        placeholder={defaultVal.phone} ref={(node)=>{this.phone = node}}/>
                    </div>

                    <input className="button-update__office" type="submit" value="Изменить" onClick={this.handleFormUpdateOffice}/>
                </form>
            </div>
        )
    }
}
