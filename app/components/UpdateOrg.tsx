import * as React from 'react';
import {Redirect} from 'react-router-dom';

export default class UpdateOrg extends React.Component<any,{}>{
    constructor(props:any){
        super(props)
    }

    name:HTMLInputElement;
    inn:HTMLInputElement;
    kpp:HTMLInputElement;
    address:HTMLInputElement;
    phone:HTMLInputElement;

    handleFormUpdateOrg = (event:any) =>{
        let dataInputs = {
            name:this.name.value,
            inn:this.inn.value,
            kpp:this.kpp.value,
            address:this.address.value,
            phone:this.phone.value
        }
        event.preventDefault();
        this.props.sendChanges(dataInputs);
    }

    componentDidMount(){
        this.props.loadOrg()
    }

    componentWillUnmount(){
        this.props.changeIsUpdated()
    }

    render(){
        const defaultVal = this.props.orgForUpdate;

        if(this.props.orgIsUpdated){
            return(
                <Redirect to="/organizations"/>
            )
        }

        return(
            <div className="updateOrg-container">
                <div className="updateOrg-head">
                    <h1>изменение данных организации</h1>
                </div>

                <form className="updateOrg-block">
                    <div>
                        <span>Название: </span>
                        <input type="text" defaultValue={defaultVal.name}
                        placeholder={defaultVal.name} ref={(node)=>{this.name = node}}/>
                        <span>ИНН: </span>
                        <input type="text" defaultValue={defaultVal.inn}
                        placeholder={defaultVal.inn} ref={(node)=>{this.inn = node}}/>
                        <span>КПП: </span>
                        <input type="text" defaultValue={defaultVal.kpp}
                        placeholder={defaultVal.kpp} ref={(node)=>{this.kpp = node}}/>
                        <span>Адрес: </span>
                        <input type="text" defaultValue={defaultVal.address}
                        placeholder={defaultVal.address} ref={(node)=>{this.address = node}}/>
                        <span>Телефон: </span>
                        <input type="text" defaultValue={defaultVal.phone}
                        placeholder={defaultVal.phone} ref={(node)=>{this.phone = node}}/>
                    </div>

                    <input className="button-update__org" type="submit" value="Изменить" onClick={this.handleFormUpdateOrg}/>
                </form>
            </div>
        )
    }
}
