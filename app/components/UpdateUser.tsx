import * as React from 'react';
import {Redirect} from 'react-router-dom';

export default class UpdateUser extends React.Component<any,{}>{
    constructor(props:any){
        super(props)
    }

    firstName:HTMLInputElement;
    secondName:HTMLInputElement;
    middleName:HTMLInputElement;
    position:HTMLInputElement;
    phone:HTMLInputElement;
    docName:HTMLInputElement;
    docNumber:HTMLInputElement;
    docDate:HTMLInputElement;
    citizenshipName:HTMLInputElement;
    citizenshipCode:HTMLInputElement;
    isIdentified:HTMLInputElement;

    handleFormUpdateUser = (event:any) =>{
        let dataInputs = {
            firstName:this.firstName.value,
            secondName:this.secondName.value,
            middleName:this.middleName.value,
            position:this.position.value,
            phone:this.phone.value,
            docName:this.docName.value,
            docNumber:this.docNumber.value,
            docDate:this.docDate.value,
            citizenshipName:this.citizenshipName.value,
            citizenshipCode:this.citizenshipCode.value,
            isIdentified:this.isIdentified.value
        }
        event.preventDefault();
        this.props.sendChanges(dataInputs);
    }

    componentDidMount(){
        this.props.loadUser()
    }

    componentWillUnmount(){
        this.props.changeIsUpdated()
    }

    render(){
        const defaultVal = this.props.userForUpdate;

        if(this.props.userIsUpdated){
            return(
                <Redirect to="/users"/>
            )
        }

        return(
            <div className="updateUser-container">
                <div className="updateUser-head">
                    <h1>изменение данных работника</h1>
                </div>

                <form className="updateUser-block">
                    <div>
                        <span>Имя: </span>
                        <input type="text" defaultValue={defaultVal.firstName}
                        placeholder={defaultVal.firstName} ref={(node)=>{this.firstName = node}}/>
                        <span>Фамилия: </span>
                        <input type="text" defaultValue={defaultVal.secondName}
                        placeholder={defaultVal.secondName} ref={(node)=>{this.secondName = node}}/>
                        <span>Отчество: </span>
                        <input type="text" defaultValue={defaultVal.middleName}
                        placeholder={defaultVal.middleName} ref={(node)=>{this.middleName = node}}/>
                        <span>Должность: </span>
                        <input type="text" defaultValue={defaultVal.position}
                        placeholder={defaultVal.position} ref={(node)=>{this.position = node}}/>
                        <span>Телефон: </span>
                        <input type="text" defaultValue={defaultVal.phone}
                        placeholder={defaultVal.phone} ref={(node)=>{this.phone = node}}/>
                        <span>docName: </span>
                        <input type="text" defaultValue={defaultVal.docName}
                        placeholder={defaultVal.docName} ref={(node)=>{this.docName = node}}/>
                        <span>docNumber: </span>
                        <input type="text" defaultValue={defaultVal.docNumber}
                        placeholder={defaultVal.docNumber} ref={(node)=>{this.docNumber = node}}/>
                        <span>docDate: </span>
                        <input type="text" defaultValue={defaultVal.docDate}
                        placeholder={defaultVal.docDate} ref={(node)=>{this.docDate = node}}/>
                        <span>citizenshipName: </span>
                        <input type="text" defaultValue={defaultVal.citizenshipName}
                        placeholder={defaultVal.citizenshipName} ref={(node)=>{this.citizenshipName = node}}/>
                        <span>citizenshipCode: </span>
                        <input type="text" defaultValue={defaultVal.citizenshipCode}
                        placeholder={defaultVal.citizenshipCode} ref={(node)=>{this.citizenshipCode = node}}/>
                        <span>isIdentified: </span>
                        <input type="text" defaultValue={defaultVal.isIdentified}
                        placeholder={defaultVal.isIdentified} ref={(node)=>{this.isIdentified = node}}/>
                    </div>


                    <input className="button-update__user" type="submit" value="Изменить" onClick={this.handleFormUpdateUser}/>
                </form>
            </div>
        )
    }
}
