import * as React from 'react';
import {Link} from 'react-router-dom';

const UserBlock = (user:any,{onclick}:any) => {
    return(
    <div className="user-item">
        <div className="user-block__head">
            <h2>{`${user.secondName} ${user.firstName} ${user.middleName}`}</h2>
        </div>
        <div className="user-body">
            <p>Должность:{user.position}</p>
            <p>Телефон:{user.phone}</p>
            <p>docName:{user.docName}</p>
            <p>docNumber:{user.docNumber}</p>
            <p>docDate:{user.docDate}</p>
            <p>citizenshipName:{user.citizenshipName}</p>
            <p>citizenshipCode:{user.citizenshipCode}</p>
        </div>
        <div className="user-buttons">
            <Link to={`/updateuser/${user.id}`}>Изменить</Link>
            <a href="" onClick={onclick}>Удалить</a>
        </div>
    </div>
    )
};

export default class Users extends React.Component<any>{
    constructor(props:any){
        super(props)

    }

    onClickDelete = ()=>{
        return this.props.deleteUser
    }

    componentWillMount(){
        this.props.loadUsers()
    }

    render() {
        return(
            <div className="user-container">
                <div className="user-head">
                    <div>
                        <h1>список работников</h1>
                    </div>
                    <div className="user-create">
                        <Link to="/createuser">cоздать работника</Link>
                    </div>
                </div>

                <div className="user_block">
                    {this.props.users.map((user:any)=>{
                        return <UserBlock key={user.id} {...user} onclick={()=>this.onClickDelete()}/>
                    })}
                </div>
            </div>

        )
    }
}
