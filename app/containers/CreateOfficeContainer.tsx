import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import CreateOffice from '../components/CreateOffice';
import {FetchCreateOffice,createdOffice} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return{
        createdOffice:state.createdOffice
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        sendCreatedOffice:(data:{}) =>{
            dispatch(FetchCreateOffice(data))
        },
        createdOff:()=>{
            dispatch(createdOffice())
        }
    }
}

const CreateOfficeContainer = connect(mapStateToProps,mapDispatchToProps)(CreateOffice);

export default CreateOfficeContainer;