import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import CreateOrg from '../components/CreateOrg';
import {FetchCreateOrg,createdOrganization} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return{
        createdOrg:state.createdOrg
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        sendCreatedOrg:(data:{}) =>{
            dispatch(FetchCreateOrg(data))
        },
        createdOrganization:()=>{
            dispatch(createdOrganization())
        }
    }
}

const CreateOrgContainer = connect(mapStateToProps,mapDispatchToProps)(CreateOrg);

export default CreateOrgContainer;