import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import CreateUser from '../components/CreateUser';
import {FetchCreateUser,createdUser} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return{
        createdUser:state.createdUser
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        sendCreatedUser:(data:{}) =>{
            dispatch(FetchCreateUser(data))
        },
        createdUsr:()=>{
            dispatch(createdUser())
        }
    }
}

const CreateUserContainer = connect(mapStateToProps,mapDispatchToProps)(CreateUser);

export default CreateUserContainer;