import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {FetchLogin} from '../redux/actions'
import Login from '../components/Login'


function mapStateToProps(state:any) {
    return {
        isLogin:state.isLogin
    }
}
  
function mapDispatchToProps(dispatch: Dispatch<any>){
    return {
        onSubmitForm: (login:any,password:any) => dispatch(FetchLogin(login.value,password.value))
    };
}
  
const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginContainer;