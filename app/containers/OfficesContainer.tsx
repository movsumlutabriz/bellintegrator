import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import Offices from '../components/Offices';
import {FetchOffices,FetchDeleteOffice} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return {
        offices:state.offices
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        loadOffices:()=>{
            dispatch(FetchOffices())
        },
        deleteOffice:()=>{
            dispatch(FetchDeleteOffice())
        }
    }
}

const OfficesContainer = connect(
    mapStateToProps,mapDispatchToProps)(Offices);

export default OfficesContainer;
