import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import Organizations from '../components/Organizations';
import {FetchOrganizations,FetchDeleteOrganization} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return {
        orgs:state.orgs
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        loadOrganizations:()=>{
            dispatch(FetchOrganizations())
        },
        deleteOrganization:()=>{
            dispatch(FetchDeleteOrganization())
        }
    }
}

const OrgContainer = connect(
    mapStateToProps,mapDispatchToProps)(Organizations);

export default OrgContainer;
