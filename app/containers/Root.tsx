import * as React from 'react';
import {Route} from 'react-router-dom';
import LoginContainer from './LoginContainer';
import OrgContainer from './OrgContainer';
import OfficesContainer from './OfficesContainer';
import UsersContainer from './UsersContainer';
import UpdOrgContainer from './UpdOrgContainer';
import CreateOrgContainer from './CreateOrgContainer';
import UpdOfficeContainer from './UpdOfficeContainer';
import CreateOfficeContainer from './CreateOfficeContainer';
import UpdUserContainer from './UpdUserContainer';
import CreateUserContainer from './CreateUserContainer';

export default class Root extends React.Component {
    render() {
        return(
            <div className="root">
                <Route exact path="/" component={LoginContainer}/>

                <Route path="/organizations" component={OrgContainer}/>
                <Route path="/updateorg/:orgid" component={UpdOrgContainer}/>
                <Route path="/createorg" component={CreateOrgContainer}/>

                <Route path="/offices" component={OfficesContainer}/>
                <Route path="/updateoffice/:officeid" component={UpdOfficeContainer}/>
                <Route path="/createoffice" component={CreateOfficeContainer}/>

                <Route path="/users" component={UsersContainer}/>
                <Route path="/updateuser/:userid" component={UpdUserContainer}/>
                <Route path="/createuser" component={CreateUserContainer}/>
            </div>
        )
    }
}
