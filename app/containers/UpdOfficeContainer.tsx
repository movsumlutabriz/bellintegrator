import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import UpdateOffice from '../components/UpdateOffice';
import {FetchOfficeById,FetchUpdateOffice,changeOfficeIsUpdated} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return {
        officeForUpdate:state.officeForUpdate,
        officeIsUpdated:state.officeIsUpdated
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        sendChanges:(data:{})=>{
            dispatch(FetchUpdateOffice(data))
        },
        loadOffice:()=>{
            dispatch(FetchOfficeById())
        },
        changeIsUpdated:()=>{
            dispatch(changeOfficeIsUpdated())
        }
    }
}

const UpdOfficeContainer = connect(
    mapStateToProps,mapDispatchToProps)(UpdateOffice);

export default UpdOfficeContainer;
