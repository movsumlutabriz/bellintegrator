import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import UpdateOrg from '../components/UpdateOrg';
import {FetchOrgById,FetchUpdateOrg,changeOrgIsUpdated} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return {
        orgForUpdate:state.orgForUpdate,
        orgIsUpdated:state.orgIsUpdated
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        sendChanges:(data:{})=>{
            dispatch(FetchUpdateOrg(data))
        },
        loadOrg:()=>{
            dispatch(FetchOrgById())
        },
        changeIsUpdated:()=>{
            dispatch(changeOrgIsUpdated())
        }
    }
}

const UpdOrgContainer = connect(
    mapStateToProps,mapDispatchToProps)(UpdateOrg);

export default UpdOrgContainer;
