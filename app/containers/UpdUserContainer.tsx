import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import UpdateUser from '../components/UpdateUser';
import {FetchUserById,FetchUpdateUser,changeUserIsUpdated} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return {
        userForUpdate:state.userForUpdate,
        userIsUpdated:state.userIsUpdated
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        sendChanges:(data:{})=>{
            dispatch(FetchUpdateUser(data))
        },
        loadUser:()=>{
            dispatch(FetchUserById())
        },
        changeIsUpdated:()=>{
            dispatch(changeUserIsUpdated())
        }
    }
}

const UpdUserContainer = connect(
    mapStateToProps,mapDispatchToProps)(UpdateUser);

export default UpdUserContainer;
