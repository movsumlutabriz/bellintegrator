import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import Users from '../components/Users';
import {FetchUsers,FetchDeleteUser} from '../redux/actions';

const mapStateToProps = (state:any) => {
    return {
        users:state.users
    }
}

const mapDispatchToProps = (dispatch:Dispatch<any>) => {
    return{
        loadUsers:()=>{
            dispatch(FetchUsers())
        },
        deleteUser:()=>{
            dispatch(FetchDeleteUser())
        }
    }
}

const UsersContainer = connect(
    mapStateToProps,mapDispatchToProps)(Users);

export default UsersContainer;