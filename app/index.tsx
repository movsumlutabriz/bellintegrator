import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {BrowserRouter as Router} from 'react-router-dom';
import Root from './containers/Root';
import configureStore from './redux/confStore'
import './styles/index.less';


const store = configureStore();

ReactDOM.render(
  <Router>
    <Provider store={store}>
      <Root/>
    </Provider>
  </Router>,
  document.getElementById('app')
);
