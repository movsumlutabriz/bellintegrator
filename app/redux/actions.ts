export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export type LOGIN_SUCCESS = typeof LOGIN_SUCCESS;

export const RECEIVE_DATA_ORGS = "RECEIVE_DATA_ORGS";
export type RECEIVE_DATA_ORGS = typeof RECEIVE_DATA_ORGS;

export const RECEIVE_DATA_ORG_FOR_UPDATE = "RECEIVE_DATA_ORG_FOR_UPDATE";
export type RECEIVE_DATA_ORG_FOR_UPDATE = typeof RECEIVE_DATA_ORG_FOR_UPDATE;

export const CHANGE_ORG_IS_UPDATED = "CHANGE_ORG_IS_UPDATED";
export type CHANGE_ORG_IS_UPDATED = typeof CHANGE_ORG_IS_UPDATED;

export const CREATED_ORGANIZATION = "CREATED_ORGANIZATION";
export type CREATED_ORGANIZATION = typeof CREATED_ORGANIZATION;

export const DELETE_ORGANIZATION = "DELETE_ORGANIZATION";
export type DELETE_ORGANIZATION = typeof DELETE_ORGANIZATION;


export const RECEIVE_DATA_OFFICES = "RECEIVE_DATA_OFFICES";
export type RECEIVE_DATA_OFFICES = typeof RECEIVE_DATA_OFFICES;

export const DELETE_OFFICE = "DELETE_OFFICE";
export type DELETE_OFFICE = typeof DELETE_OFFICE;

export const RECEIVE_DATA_OFFICE_FOR_UPDATE = "RECEIVE_DATE_OFFICE_FOR_UPDATE";
export type RECEIVE_DATA_OFFICE_FOR_UPDATE = typeof RECEIVE_DATA_OFFICE_FOR_UPDATE;

export const CREATED_OFFICE = "CREATED_OFFICE";
export type CREATED_OFFICE = typeof CREATED_OFFICE;

export const CHANGE_OFFICE_IS_UPDATED = "CHANGE_OFFICE_IS_UPDATED";
export type CHANGE_OFFICE_IS_UPDATED = typeof CHANGE_OFFICE_IS_UPDATED;



export const RECEIVE_DATA_USERS = "RECEIVE_DATA_USERS";
export type RECEIVE_DATA_USERS = typeof RECEIVE_DATA_USERS;

export const DELETE_USER = "DELETE_USER";
export type DELETE_USER = typeof DELETE_USER;

export const RECEIVE_DATA_USER_FOR_UPDATE = "RECEIVE_DATA_USER_FOR_UPDATE";
export type RECEIVE_DATA_USER_FOR_UPDATE = typeof RECEIVE_DATA_USER_FOR_UPDATE;

export const CREATED_USER = "CREATED_USER";
export type CREATED_USER = typeof CREATED_USER;

export const CHANGE_USER_IS_UPDATED = "CHANGE_USER_IS_UPDATED";
export type CHANGE_USER_IS_UPDATED = typeof CHANGE_USER_IS_UPDATED;

interface LoginSuccess{
    type:LOGIN_SUCCESS,
    newData:boolean
}

interface receiveDataOrgs{
    type:RECEIVE_DATA_ORGS,
    newData:any[]
}

interface receiveDataOrgForUpdate{
    type:RECEIVE_DATA_ORG_FOR_UPDATE,
    newData:{}
}

interface changeOrgIsUpdated{
    type:CHANGE_ORG_IS_UPDATED
}

interface createdOrganization{
    type:CREATED_ORGANIZATION
}

interface deleteOrganization{
    type:DELETE_ORGANIZATION
}




interface receiveDataOffices{
    type:RECEIVE_DATA_OFFICES,
    newData:any[]
}

interface receiveDataOfficeForUpdate{
    type:RECEIVE_DATA_OFFICE_FOR_UPDATE,
    newData:{}
}

interface deleteOffice{
    type:DELETE_OFFICE
}

interface createdOffice{
    type:CREATED_OFFICE
}

interface changeOfficeIsUpdated{
    type:CHANGE_OFFICE_IS_UPDATED
}




interface receiveDataUsers{
    type:RECEIVE_DATA_USERS,
    newData:any[]
}

interface receiveDataUserForUpdate{
    type:RECEIVE_DATA_USER_FOR_UPDATE,
    newData:{}
}

interface deleteUser{
    type:DELETE_USER
}

interface createdUser{
    type:CREATED_USER
}

interface changeUserIsUpdated{
    type:CHANGE_USER_IS_UPDATED
}

export function LoginSuccess(data:any):LoginSuccess{
    return{
        type:LOGIN_SUCCESS,
        newData:data.result
    }
}

export function receiveDataOrgs(data:any[]):receiveDataOrgs{
    return{
        type:RECEIVE_DATA_ORGS,
        newData:data
    }
}

export function receiveDataOrgForUpdate(data:{}):receiveDataOrgForUpdate{
    return{
        type:RECEIVE_DATA_ORG_FOR_UPDATE,
        newData:data
    }
}

export function changeOrgIsUpdated():changeOrgIsUpdated{
    return{
        type:CHANGE_ORG_IS_UPDATED
    }
}

export function createdOrganization():createdOrganization{
    return{
        type:CREATED_ORGANIZATION
    }
}

export function deleteOrganization():deleteOrganization{
    return{
        type:DELETE_ORGANIZATION
    }
}






export function receiveDataOffices(data:any[]):receiveDataOffices{
    return{
        type:RECEIVE_DATA_OFFICES,
        newData:data
    }
}

export function deleteOffice():deleteOffice{
    return{
        type:DELETE_OFFICE
    }
}

export function receiveDataOfficeForUpdate(data:any):receiveDataOfficeForUpdate{
    return{
        type:RECEIVE_DATA_OFFICE_FOR_UPDATE,
        newData:data
    }
}

export function createdOffice():createdOffice{
    return{
        type:CREATED_OFFICE
    }
}

export function changeOfficeIsUpdated():changeOfficeIsUpdated{
    return{
        type:CHANGE_OFFICE_IS_UPDATED
    }
}





export function receiveDataUsers(data:any[]):receiveDataUsers{
    return{
        type:RECEIVE_DATA_USERS,
        newData:data
    }
}

export function deleteUser():deleteUser{
    return{
        type:DELETE_USER
    }
}

export function receiveDataUserForUpdate(data:any):receiveDataUserForUpdate{
    console.log(data);
    return{
        type:RECEIVE_DATA_USER_FOR_UPDATE,
        newData:data
    }
}

export function createdUser():createdUser{
    return{
        type:CREATED_USER
    }
}

export function changeUserIsUpdated():changeUserIsUpdated{
    return{
        type:CHANGE_USER_IS_UPDATED
    }
}


const headersFetchJson = new Headers();
headersFetchJson.append("Content-Type","application/json");

export function FetchLogin(login:any,password:any) {
    return (dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5aae919f2f00005600273d85',{
            method:'POST',
            headers:headersFetchJson,
            body:JSON.stringify({
                login:login,
                password:password
            })
        })
        .then(response=>response.json())
        .then(json=>{dispatch(LoginSuccess(json))})
    }
}

export function FetchOrganizations() {
    return (dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5abcc8882f00005700e6c11f')
        .then(response=>response.json())
        .then(json=>{dispatch(receiveDataOrgs(json))})
    }
}

export function FetchOrgById(){
    return(dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5abcc9302f00006300e6c121')
        .then(response=>response.json())
        .then(json=>{dispatch(receiveDataOrgForUpdate(json))})
    }
}

export function FetchUpdateOrg(data:any){
    return(dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5ab5456b3000004c00827948',{
            method:'POST',
            headers:headersFetchJson,
            body:JSON.stringify({
                name:data.name.value,
                inn:data.inn.value,
                kpp:data.kpp.value,
                address:data.address.value,
                phone:data.phone.value
            })
        })
        .then(dispatch(changeOrgIsUpdated()))
    }
}

export function FetchDeleteOrganization(){
    return(dispatch:any)=>{
        return fetch('')
        .then(dispatch(FetchOrganizations()))
    }
}

export function FetchCreateOrg(data:any){
    return(dispatch:any)=>{
        return fetch('',{
            method:'POST',
            headers:headersFetchJson,
            body:JSON.stringify({
                name:data.name.value,
                inn:data.inn.value,
                kpp:data.kpp.value,
                address:data.address.value,
                phone:data.phone.value
            })
        })
        .then(dispatch(createdOrganization()))
    }
}




export function FetchOffices() {
    return (dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5abf33bd2c00005a00c3cdda')
        .then(response=>response.json())
        .then(json=>{dispatch(receiveDataOffices(json))})
    }
}

export function FetchDeleteOffice(){
    return(dispatch:any)=>{
        return fetch('')
        .then(dispatch(FetchOffices()))
    }
}

export function FetchOfficeById(){
    return(dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5abcca5e2f00006000e6c125')
        .then(response=>response.json())
        .then(json=>{dispatch(receiveDataOfficeForUpdate(json))})
    }
}

export function FetchUpdateOffice(data:any){
    return(dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5ab78b113400004d0067abed',{
            method:'POST',
            headers:headersFetchJson,
            body:JSON.stringify({
                name:data.name.value,
                address:data.address.value,
                phone:data.phone.value
            })
        })
        .then(dispatch(changeOfficeIsUpdated()))
    }
}

export function FetchCreateOffice(data:any){
    return(dispatch:any)=>{
        return fetch('',{
            method:'POST',
            headers:headersFetchJson,
            body:JSON.stringify({
                name:data.name.value,
                address:data.address.value,
                phone:data.phone.value
            })
        })
        .then(dispatch(createdOffice()))
    }
}

export function FetchUsers() {
    return (dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5abccb652f00005800e6c129')
        .then(response=>response.json())
        .then(json=>{dispatch(receiveDataUsers(json))})
    }
}

export function FetchDeleteUser() {
    return(dispatch:any)=>{
        return fetch('')
        .then(dispatch(FetchUsers()))
    }
}

export function FetchUserById(){
    return(dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5abccb8e2f00000f00e6c12a')
        .then(response=>response.json())
        .then(json=>{dispatch(receiveDataUserForUpdate(json))})
    }
}

export function FetchUpdateUser(data:any){
    return(dispatch:any) => {
        return fetch('http://www.mocky.io/v2/5ab78b113400004d0067abed',{
            method:'POST',
            headers:headersFetchJson,
            body:JSON.stringify({
                firstName:data.firstName.value,
                secondName:data.secondName.value,
                middleName:data.middleName.value,
                position:data.position.value,
                phone:data.phone.value,
                docName:data.docName.value,
                docNumber:data.docNumber.value,
                docDate:data.docDate.value,
                citizenshipName:data.citizenshipName.value,
                citizenshipCode:data.citizenshipCode.value,
                isIdentified:data.isIdentified.value
            })
        })
        .then(dispatch(changeUserIsUpdated()))
    }
}

export function FetchCreateUser(data:any){
    return(dispatch:any)=>{
        return fetch('',{
            method:'POST',
            headers:headersFetchJson,
            body:JSON.stringify({
                firstName:data.firstName.value,
                secondName:data.secondName.value,
                middleName:data.middleName.value,
                position:data.position.value,
                phone:data.phone.value,
                docName:data.docName.value,
                docNumber:data.docNumber.value,
                docDate:data.docDate.value,
                citizenshipName:data.citizenshipName.value,
                citizenshipCode:data.citizenshipCode.value,
                isIdentified:data.isIdentified.value
            })
        })
        .then(dispatch(createdUser()))
    }
}
