import {Action} from 'redux';
import {LOGIN_SUCCESS,

        RECEIVE_DATA_ORGS,
        RECEIVE_DATA_ORG_FOR_UPDATE,
        CHANGE_ORG_IS_UPDATED,
        CREATED_ORGANIZATION,

        RECEIVE_DATA_OFFICES,
        RECEIVE_DATA_OFFICE_FOR_UPDATE,
        CHANGE_OFFICE_IS_UPDATED,
        CREATED_OFFICE,

        RECEIVE_DATA_USERS,
        RECEIVE_DATA_USER_FOR_UPDATE,
        CHANGE_USER_IS_UPDATED,
        CREATED_USER


} from './actions';

interface IActionType extends Action{
    type:any,
    newData:any
}

interface IStoreState{
    isLogin:boolean,
    orgIsUpdated:boolean,
    officeIsUpdated:boolean,
    userIsUpdated:boolean,
    createdOrg:boolean,
    createdOffice:boolean,
    createdUser:boolean,
    orgs:{},
    orgForUpdate:{}
    offices:{},
    officeForUpdate:{},
    users:{},
    userForUpdate:{}
}

const initialState = {
    get state():IStoreState{
        return {
            isLogin:false,
            orgIsUpdated:false,
            officeIsUpdated:false,
            userIsUpdated:false,
            createdOrg:false,
            createdOffice:false,
            createdUser:false,
            orgs:[],
            orgForUpdate:{},
            offices:[],
            officeForUpdate:{},
            users:[],
            userForUpdate:{}
        }
    }
}

export default function reducer(state:IStoreState = initialState.state, action:IActionType) {
    switch (action.type) {
        case LOGIN_SUCCESS:
            return{
                ...state,
                isLogin:action.newData
            }


        case RECEIVE_DATA_ORGS:
            return{
                ...state,
                orgs:action.newData
            }
        case RECEIVE_DATA_ORG_FOR_UPDATE:
            return{
                ...state,
                orgForUpdate:action.newData
            }
        case CHANGE_ORG_IS_UPDATED:
            return{
                ...state,
                orgIsUpdated:!state.orgIsUpdated
            }
        case CREATED_ORGANIZATION:
            return{
                ...state,
                createdOrg:!state.createdOrg
            }


        case RECEIVE_DATA_OFFICES:
            return{
                ...state,
                offices:action.newData
            }
        case RECEIVE_DATA_OFFICE_FOR_UPDATE:
            return{
                ...state,
                officeForUpdate:action.newData
            }
        case CREATED_OFFICE:
            return{
                ...state,
                createdOffice:!state.createdOffice
            }
        case CHANGE_OFFICE_IS_UPDATED:
            return{
                ...state,
                officeIsUpdated:!state.officeIsUpdated
            }



        case RECEIVE_DATA_USERS:
            return{
                ...state,
                users:action.newData
            }
        case RECEIVE_DATA_USER_FOR_UPDATE:
            return{
                ...state,
                userForUpdate:action.newData
            }
        case CREATED_USER:
            return{
                ...state,
                createdUser:!state.createdUser
            }
        case CHANGE_USER_IS_UPDATED:
            return{
                ...state,
                userIsUpdated:!state.userIsUpdated
            }


        default:
            return state
    }

}
